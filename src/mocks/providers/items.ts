import { Injectable } from '@angular/core';

import { Item } from '../../models/item';

@Injectable()
export class Items {
  itemsDone: any = false;
  percentaje: any = 0;
  items: Item[] = [];
  defaultItem: any = {
    "name": "5 Plátanos para hoy",
    "profilePic": "https://estaticos.muyinteresante.es/media/cache/1000x460_thumb/uploads/images/gallery/579f2df65bafe8178b8b456d/platano2.jpg",
    "about": "Esté alimento proporciona potasio.",
    "done": false
  };

  constructor() {
    let items = [
      {
        "name": "5 Plátanos para hoy",
        "profilePic": "https://estaticos.muyinteresante.es/media/cache/1000x460_thumb/uploads/images/gallery/579f2df65bafe8178b8b456d/platano2.jpg",
        "about": "Un plátano de unos 100 gramos contiene aproximadamente 100 calorías, 1 gramo de proteína, 25 gramos de hidratos de carbono y menos de un gramo de grasa. También contiene 3 gramos de fibra. El plátano es rico en vitaminas B6 y C, ácido fólico y minerales como el magnesio y el potasio.",
        "done": false
      },
      {
        "name": "1 Manzana al día",
        "profilePic": "http://muyenforma.com/wp-content/uploads/2015/02/dieta-manzana.jpg",
        "about": "1. Las manzanas contienen una fibra soluble llamada pectina, que ayuda a reducir el colesterol en la sangre, y evita que se acumule en las paredes de los vasos sanguíneos. 2. También contienen grandes cantidades de potasio, mineral que ayuda a controlar la presión arterial, y puede reducir el riesgo de un ataque cardiaco. 3. La quercetina es un poderoso antioxidante que también se encuentra en las manzanas, y está demostrado que ayuda a proteger las neuronas del cerebro humano. 4. Al fermentarse en el colón, la fibra natural de las manzanas produce químicos que ayudan a evitar la formación de células cancerígenas. 5. Su consumo también puede ayudar a las personas que sufren de asma. Los flavonoides y ácidos fenólicos que contiene, ayudan a calmar la inflamación de las vías respiratorias. 6. Otro de sus flavonoides, llamado floridzina, protege los huesos de enfermedades como la osteoporosis, y puede incrementar su fortalecimiento y densidad. 7. Es considerada una fruta diurética, debido a la concentración de potasio que contiene. Muy recomendable para personas que sufren de retención de líquidos. 8. Además, se recomienda su consumo para prevenir el estreñimiento o la diarrea. La cáscara de manzana contiene una fibra insoluble que ayuda la evacuación intestinal. 9. Ayuda a que los dientes se vean más limpios, porque al comerla, barre restos de otros alimentos. También se dice que es muy buena para prevenir la caries.",
        "done": false
      },
      {
        "name": "7 Almendras para este día",
        "profilePic": "http://sm.askmen.com/askmen_latam/photo/default/almendras-alimentos-para-bajar-la-ansiedad-bajar-de-peso_pdga.jpg",
        "about": "En general todas las oleaginosas, son alimentos que nos aportan importantes cantidades de fibra, proteínas, minerales, vitaminas del grupo B, vitamina E, y grasas saludables. Entre los minerales destacan hierro, fósforo, magnesio, potasio, zinc y calcio..",
        "done": false
      },
      {
        "name": "1 Pescado que delicia",
        "profilePic": "https://cdn.shopify.com/s/files/1/0555/6477/files/Embelle-TratamientoGOLD-Recetasconpescado_grande.jpg?14057681501268934163",
        "about": "Rico en proteínas de muy alto valor nutritivo aportándonos todos los aminoácidos esenciales necesarios para formar y mantener los órganos, tejidos y el sistema de defensa frente a infecciones y agentes externos.",
        "done": false
      },
      {
        "name": "Un vaso de Leche",
        "profilePic": "http://walac.pe/wp-content/uploads/2017/06/leche-segundoenfoque.jpg",
        "about": "La riqueza de las propiedades de la leche. La leche, al contrario que otras bebidas derivadas de vegetales, presenta una variedad nutricional muy rica porque contiene calcio, fósforo, magnesio, zinc, yodo, selenio y vitaminas A, D y del complejo B; presenta una cantidad muy alta de vitamina B12.",
        "done": false
      },

      {
        "name": "1 Manzana al día",
        "profilePic": "http://muyenforma.com/wp-content/uploads/2015/02/dieta-manzana.jpg",
        "about": "1. Las manzanas contienen una fibra soluble llamada pectina, que ayuda a reducir el colesterol en la sangre, y evita que se acumule en las paredes de los vasos sanguíneos. 2. También contienen grandes cantidades de potasio, mineral que ayuda a controlar la presión arterial, y puede reducir el riesgo de un ataque cardiaco. 3. La quercetina es un poderoso antioxidante que también se encuentra en las manzanas, y está demostrado que ayuda a proteger las neuronas del cerebro humano. 4. Al fermentarse en el colón, la fibra natural de las manzanas produce químicos que ayudan a evitar la formación de células cancerígenas. 5. Su consumo también puede ayudar a las personas que sufren de asma. Los flavonoides y ácidos fenólicos que contiene, ayudan a calmar la inflamación de las vías respiratorias. 6. Otro de sus flavonoides, llamado floridzina, protege los huesos de enfermedades como la osteoporosis, y puede incrementar su fortalecimiento y densidad. 7. Es considerada una fruta diurética, debido a la concentración de potasio que contiene. Muy recomendable para personas que sufren de retención de líquidos. 8. Además, se recomienda su consumo para prevenir el estreñimiento o la diarrea. La cáscara de manzana contiene una fibra insoluble que ayuda la evacuación intestinal. 9. Ayuda a que los dientes se vean más limpios, porque al comerla, barre restos de otros alimentos. También se dice que es muy buena para prevenir la caries.",
        "done": false
      },
      {
        "name": "7 Almendras para este día",
        "profilePic": "http://sm.askmen.com/askmen_latam/photo/default/almendras-alimentos-para-bajar-la-ansiedad-bajar-de-peso_pdga.jpg",
        "about": "En general todas las oleaginosas, son alimentos que nos aportan importantes cantidades de fibra, proteínas, minerales, vitaminas del grupo B, vitamina E, y grasas saludables. Entre los minerales destacan hierro, fósforo, magnesio, potasio, zinc y calcio..",
        "done": false
      },
      {
        "name": "1 Pescado que delicia",
        "profilePic": "https://cdn.shopify.com/s/files/1/0555/6477/files/Embelle-TratamientoGOLD-Recetasconpescado_grande.jpg?14057681501268934163",
        "about": "Rico en proteínas de muy alto valor nutritivo aportándonos todos los aminoácidos esenciales necesarios para formar y mantener los órganos, tejidos y el sistema de defensa frente a infecciones y agentes externos.",
        "done": false
      },
      {
        "name": "Un vaso de Leche",
        "profilePic": "http://walac.pe/wp-content/uploads/2017/06/leche-segundoenfoque.jpg",
        "about": "La riqueza de las propiedades de la leche. La leche, al contrario que otras bebidas derivadas de vegetales, presenta una variedad nutricional muy rica porque contiene calcio, fósforo, magnesio, zinc, yodo, selenio y vitaminas A, D y del complejo B; presenta una cantidad muy alta de vitamina B12.",
        "done": false
      },
      
      {
        "name": "1 Manzana al día",
        "profilePic": "http://muyenforma.com/wp-content/uploads/2015/02/dieta-manzana.jpg",
        "about": "1. Las manzanas contienen una fibra soluble llamada pectina, que ayuda a reducir el colesterol en la sangre, y evita que se acumule en las paredes de los vasos sanguíneos. 2. También contienen grandes cantidades de potasio, mineral que ayuda a controlar la presión arterial, y puede reducir el riesgo de un ataque cardiaco. 3. La quercetina es un poderoso antioxidante que también se encuentra en las manzanas, y está demostrado que ayuda a proteger las neuronas del cerebro humano. 4. Al fermentarse en el colón, la fibra natural de las manzanas produce químicos que ayudan a evitar la formación de células cancerígenas. 5. Su consumo también puede ayudar a las personas que sufren de asma. Los flavonoides y ácidos fenólicos que contiene, ayudan a calmar la inflamación de las vías respiratorias. 6. Otro de sus flavonoides, llamado floridzina, protege los huesos de enfermedades como la osteoporosis, y puede incrementar su fortalecimiento y densidad. 7. Es considerada una fruta diurética, debido a la concentración de potasio que contiene. Muy recomendable para personas que sufren de retención de líquidos. 8. Además, se recomienda su consumo para prevenir el estreñimiento o la diarrea. La cáscara de manzana contiene una fibra insoluble que ayuda la evacuación intestinal. 9. Ayuda a que los dientes se vean más limpios, porque al comerla, barre restos de otros alimentos. También se dice que es muy buena para prevenir la caries.",
        "done": false
      },
      {
        "name": "7 Almendras para este día",
        "profilePic": "http://sm.askmen.com/askmen_latam/photo/default/almendras-alimentos-para-bajar-la-ansiedad-bajar-de-peso_pdga.jpg",
        "about": "En general todas las oleaginosas, son alimentos que nos aportan importantes cantidades de fibra, proteínas, minerales, vitaminas del grupo B, vitamina E, y grasas saludables. Entre los minerales destacan hierro, fósforo, magnesio, potasio, zinc y calcio..",
        "done": false
      },
      {
        "name": "1 Pescado que delicia",
        "profilePic": "https://cdn.shopify.com/s/files/1/0555/6477/files/Embelle-TratamientoGOLD-Recetasconpescado_grande.jpg?14057681501268934163",
        "about": "Rico en proteínas de muy alto valor nutritivo aportándonos todos los aminoácidos esenciales necesarios para formar y mantener los órganos, tejidos y el sistema de defensa frente a infecciones y agentes externos.",
        "done": false
      },
      {
        "name": "Un vaso de Leche",
        "profilePic": "http://walac.pe/wp-content/uploads/2017/06/leche-segundoenfoque.jpg",
        "about": "La riqueza de las propiedades de la leche. La leche, al contrario que otras bebidas derivadas de vegetales, presenta una variedad nutricional muy rica porque contiene calcio, fósforo, magnesio, zinc, yodo, selenio y vitaminas A, D y del complejo B; presenta una cantidad muy alta de vitamina B12.",
        "done": false
      },
    ];

    for (let item of items) {
      this.items.push(new Item(item));
    }
    this.getPercentaje();
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  countDone() {
    let done = this.items.filter((item) => {
        return item['done'] ? item : false;
    });
    this.itemsDone = done.length;
  }
  getPercentaje() {
    this.countDone();
    
    this.percentaje = (this.itemsDone * 100) / this.items.length;
  }
  add(item: Item) {
    this.items.push(item);
  }

  isDone() {
    let done = this.items.filter((item) => {
        return item['done'] ? item : false;
    });
    if(done.length == this.items.length)
      return true;

    return false;
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }

  done(item: Item) {
    this.items[this.items.indexOf(item)]['done'] = true;
  }
}

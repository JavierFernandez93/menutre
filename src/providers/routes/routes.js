var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the RoutesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var Routes = /** @class */ (function () {
    function Routes(http) {
        this.http = http;
        this.protocol = 'http';
        // path:any = 'localhost:8888/welcomeweb/app/application';
        this.path = 'www.iswelcome.com/app/application';
        this.headers = new Headers();
    }
    Routes.prototype.getProvider = function (call, callback, data) {
        if (call != undefined) {
            this.http.post(this.protocol + '://' + this.path + '/' + call.url, JSON.stringify(data), { headers: this.headers }).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (typeof callback == 'function')
                    callback(data);
            });
        }
    };
    Routes = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], Routes);
    return Routes;
}());
export { Routes };
//# sourceMappingURL=routes.js.map
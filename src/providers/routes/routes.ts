import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the RoutesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Routes {
  protocol:string = 'http';
  path:any = 'localhost:8888/welcomeweb/app/application';
  // path:any = 'www.iswelcome.com/app/application';
  headers:any;
  constructor(private http: Http){
    this.headers = new Headers();
  }

  getProvider(call:any,callback:any,data:any){
    if(call != undefined)
    {
      this.http.post(this.protocol+'://'+this.path+'/'+call.url, JSON.stringify(data), {headers: this.headers}).map(res => res.json()).subscribe(data => {
        if(typeof callback == 'function') callback(data)
      });
  	}
  }
}
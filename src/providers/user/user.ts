import { Routes } from '../routes/routes';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

// import { PhotoViewer } from '@ionic-native/photo-viewer'
import { OneSignal } from '@ionic-native/onesignal';
import { Platform, AlertController, LoadingController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Storage } from '@ionic/storage';


@Injectable()
export class User {
  x:any = false;
  OneSignal:any = false;
  oneSignalProyectId:any = false;
  tap_max:any = 3;
  tap:any = 0;
  googleAppId:any = false;
  logged:boolean = false;
  registration_id:any = false;
  userObject:any = false;
  /* fast vars */
  is_able_to_notification:any = false;
  is_able_to_tracking:any = false;
  tracking_time:any = 10000;
  instant_chat_configured:any = false;
  subscription:any = false;
  birthdate:any = false;
  short_name:any = false;
  commerces_near:any = false;
  gold_coin:any = false;
  range:any = false;
  commerce_status:any = 0;
  commerce_type:any = 0;
  reward:any = 0;
  wallet:any = 0;
  kind:any = false;
  silver_coin:any = false;
  image:any = false;
  commerce_id:any = false;
  names:any = false;
  mail:any = false;
  responseOneSignal:any = false;
  pages: Array<{title: string, icon:string, component: any, logout:boolean, subscription:any, showBadge:boolean, badge:any, additionalData:any, root:any}>;
  constructor(public loadingCtrl: LoadingController,private storage:Storage,private platform:Platform, private alertCtrl: AlertController,private diagnostic: Diagnostic,private oneSignal: OneSignal,public http: Http,public routes: Routes) {
    this.init((response)=>{
      if(response)
      {
        this.saveRegistration({},(d)=>{
        });
      }
    });
  }
  
  init(callback:any){
    this.OneSignal = false;
    this.oneSignalProyectId = "f11df8cc-74a7-48a6-869e-e5cf4e1c88b8";
    this.googleAppId = "593000169379";
    this.logged = false;
    this.birthdate = false;
    this.registration_id = 1;
    this.userObject = false;
    this.is_able_to_notification = false;
    this.is_able_to_tracking = false;
    this.subscription = false;
    this.short_name = 'Usuario';
    this.commerces_near = false;
    this.range = false;
    this.gold_coin = false;
    this.reward = 0;
    this.wallet = 0;
    this.kind = false;
    this.silver_coin = false;
    this.image = 'assets/img/upload_photo.png';
    this.names = 'Usuario';
    this.mail = false;
    this.responseOneSignal = false;

    if(callback) callback(true);
  }
  
  resetTap() {
    this.tap = 0;
  }

  needMiaHelp() {
    console.log("NEED MIA HELP");
  }

  signup(accountInfo: any) {
    // let seq = this.api.post('signup', accountInfo).share();

    // seq.subscribe((res: any) => {
    //   // If the API returned a successful response, mark the user as logged in
    //   if (res.status == 'success') {
    //     this._loggedIn(res);
    //   }
    // }, err => {
    //   console.error('ERROR', err);
    // });

    return true;
  }

  login(accountInfo: any) {

    return true;
  }

  tapEvent(event) {
    setTimeout(()=>{
      this.resetTap();
    },500);

    this.tap++;

    if(this.tap == this.tap_max)
    {
      this.needMiaHelp();
      this.resetTap();
    }
  }

  isLogged = () : any => {
   this.getUser(()=>{});
   return this.logged;
  }

  getCredentials(data:any){
    let defaults:any = false;
    
    new Promise<string>((resolve,reject)=>{
      defaults = {
        mail:this.userObject.user_login.mail,
        password:this.userObject.user_login.password,
        company_id:this.userObject.user_login.company_id
      };
      
      if(data) Object.assign(defaults,data);
      
      resolve();
    });
    
    return defaults;
  }

  getAllForMap = (callback:any,data:any) : any =>{
    return this.routes.getProvider({url:'get_all_commerces_for_map.php'},callback,data);
  }
  
  logout(callback:any){
    this.subscription = false;
    this.commerce_id = false;
    this.kind = false;
    this.image = 'assets/img/upload_photo.png';
    this.names = 'Usuario';

    this.storage.remove('userObject').then(()=>{
      this.storage.remove('prewelcome').then(()=>{
        this.storage.remove('dish_order_per_user').then(()=>{
          this.storage.remove('order_per_user_id').then(()=>{
            if(callback()) callback(true);
          });
        });
      });
    });
  }

  setInstantChatHasConfigured = () : any =>{
    this.storage.set("instant_chat_configured",true).then(() =>{});
  }
  
  hasInstantChatConfigured = (callback:any) : any =>{
    this.storage.get("instant_chat_configured").then((instant_chat_configured) => {
      this.instant_chat_configured = instant_chat_configured ? instant_chat_configured : this.instant_chat_configured;
      if(callback != undefined) callback(this.instant_chat_configured);
    });
  }

  setAmountOfCommerces = (commerces_near:any,callback:any) : any =>{
    this.storage.get("userObject").then((data) => {
      this.commerces_near = commerces_near;
      data.commerces_near = commerces_near;

      this.setUser(data,(reponse)=>{
        callback(true);
      })
    });
  }

  getAccountCreated = (callback:any) : any =>{
    this.storage.get("account_created").then(
      data => {
        if(data != null)
        {
          if(callback != undefined) callback(true);
        } else {
          if(callback != undefined) callback(false);
        }
      }
    );
  }

  setTutoAsViewed = (callback:any,tuto:any) : any =>{
    this.storage.set(tuto , true).then(() =>{
        if(callback != undefined) callback(true);
      }
    );
  }

  getTutoAsViewed = (callback:any,tuto) : any =>{
    this.storage.get(tuto).then(
      data => {
        if(data != null)
        {
          if(callback != undefined) callback(data);
        } else {
          if(callback != undefined) callback(false);
        }
      }
    );
  }

  getAllPromoMine = (callback:any,data:any) : any =>{
    return this.routes.getProvider({url:'get_all_my_commerces.php'},callback,data);
  }

  setCommerceStatus = (callback:any,status:any) : any =>{
    this.storage.set("commerce_active" , status).then(() =>{
        if(callback != undefined) callback(true);
      }
    );
  }

  getCommerceStatus = (callback:any) : any =>{
    this.storage.get("commerce_active").then(
      data => {
        if(data != null)
        {
          if(callback != undefined) callback(data);
        } else {
          if(callback != undefined) callback(false);
        }
      }
    );
  }
  setAccountCreated = (callback:any) : any =>{
    this.storage.set("account_created" , true).then(() =>{
        if(callback != undefined) callback(true);
      }
    );
  }
  getAccountEmail = (callback:any) : any =>{
    this.storage.get("email").then(
      data => {
        if(data != null)
        {
          if(callback != undefined) callback(data);
        } else {
          if(callback != undefined) callback(false);
        }
      }
    );
  }
  setSpecialDish = (dish:any,callback:any) : any =>{
    this.getSpecialDishes((response)=>{
      let dishes = [];

      if(response)
      {
        dishes = response;
      }

      dishes.push(dish);

      this.storage.set("dishes" , dishes).then(() =>{
          if(callback != undefined) callback(true);
        }
      );
    })
  }
  getSpecialDishes = (callback:any) : any =>{
    this.storage.get("dishes").then(
      data => {
        if(data != null)
        {
          if(callback != undefined) callback(data);
        } else {
          if(callback != undefined) callback(false);
        }
      }
    );
  }
  setDishOrderPerUser = (dish_order_per_user:any,callback:any) : any =>{
    this.storage.set("dish_order_per_user" , dish_order_per_user).then(() =>{
        if(callback != undefined) callback(true);
      }
    );
  }
  getDishOrderPerUser = (callback:any) : any =>{
    this.storage.get("dish_order_per_user").then(
      data => {
        if(data != null)
        {
          if(callback != undefined) callback(data);
        } else {
          if(callback != undefined) callback(false);
        }
      }
    );
  }
  setOrderPerUserId = (order_per_user_id:any,callback:any) : any =>{
    this.storage.set("order_per_user_id" , order_per_user_id).then(() =>{
        if(callback != undefined) callback(true);
      }
    );
  }
  getOrderPerUserId = (callback:any) : any =>{
    this.storage.get("order_per_user_id").then(
      data => {
        if(data != null)
        {
          if(callback != undefined) callback(data);
        } else {
          if(callback != undefined) callback(false);
        }
      }
    );
  }
  setPrewelcome = (callback:any) : any =>{
    this.storage.set("prewelcome",true).then(() =>{
        if(callback != undefined) callback(true);
      }
    );
  }
  deleteOrderPerUserId = (callback:any) : any =>{
    this.storage.set("order_per_user_id",false).then(() =>{
        if(callback != undefined) callback(true);
      }
    );
  }

  deletePrewelcome = (callback:any) : any =>{
    this.storage.set("prewelcome",false).then(() =>{
        if(callback != undefined) callback(true);
      }
    );
  }

  getPrewelcome = (callback:any) : any =>{
    this.storage.get("prewelcome").then(
      data => {
        if(data != null)
        {
          if(callback != undefined) callback(data);
        } else {
          if(callback != undefined) callback(false);
        }
      }
    );
  }

  setAccountEmail = (email:any,callback:any) : any =>{
    this.storage.set("email" , email).then(() =>{
        if(callback != undefined) callback(true);
      }
    );
  }

  setUser = (data:any,callback:any) : any =>{
    this.storage.set("userObject" , data).then(() =>{
      this.setAccountCreated((response)=>{
        this.setAccountEmail(data.user_login.mail,(response)=>{
          this.userObject = data;
          this.logged = true;

          this.setDefaults((response)=>{
            if(callback != undefined) callback(this.userObject);
          });
        });
      });
      }
    );
  }

  setUserInBoardFromPrewelcome = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'set_user_in_a_board_from_prewelcome.php'},callback,data);
  }

  acceptCommerceAuction = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'accept_auction_per_commerce.php'},callback,data);
  }

  getPaymentLog = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_payment_log.php'},callback,data);
  }

  loginWithFacebook = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'login_user_with_facebook.php'},callback,data);
  }

  loginUser = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'login_user.php'},callback,data);
  }

  updateUser = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'update_user.php'},callback,data);
  }

  updateUserPhoto = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'update_user_photo.php'},callback,data);
  }

  deleteDish = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'delete_dish_user.php'},callback,data);
  }

  saveBuyForReload = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'save_buy_for_reload.php'},callback,data);
  }

  getCommercesForSearch = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_all_commerces_for_search.php'},callback,data);
  }

  getDishesByKind = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_dishes_by_kind.php'},callback,data);
  }

  getAmountOfDinner = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_amount_of_dinner.php'},callback,data);
  }

  getAllMyFabs = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_all_my_fabs.php'},callback,data);
  }

  getAllForSearch = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'make_fab.php'},callback,data);
  }

  makeFab = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'make_fab.php'},callback,data);
  }

  getAllMyHistory = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_all_my_history.php'},callback,data);
  }

  registratePayPalBuy = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'registrate_paypal_buy.php'},callback,data);
  }

  addToCart = (callback: any,data: any) : any => {  
    return this.routes.getProvider({url:'add_dish_to_cart.php'},callback,data);
  }

  saveFastCommerce = (callback: any,data: any) : any => {  
    return this.routes.getProvider({url:'save_fast_commerce.php'},callback,data);
  }
  
  saveBuy = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'save_buy.php'},callback,data);
  }

  makeUserAuction = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'make_auction_user.php'},callback,data);
  }

  deleteAuction = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'delete_auction.php'},callback,data);
  }

  callToWaiter = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'call_to_waiter.php'},callback,data);
  }

  getReload = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'regenerate_saved_reload.php'},callback,data);
  }

  getBuy = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'regenerate_saved_buy.php'},callback,data);
  }

  getShortMenu = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_short_menu.php'},callback,data);
  }

  setCommerceAsOpened = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'set_commerce_as_opened.php'},callback,data);
  }

  getAll = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_all_commerces.php'},callback,data);
  }
  
  getAllUserAuctions = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_all_auction_user.php'},callback,data);
  }

  updateReamingTime = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'update_reaming_time.php'},callback,data);
  }

  getCatalogActivity = (callback:any,data:any) : any =>{
    return this.routes.getProvider({url:'get_catalog_activiy.php'},callback,data);
  }

  getAllUserActiveAuctions = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_all_auction_user_active.php'},callback,data);
  }

  resetOrder = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'reset_order.php'},callback,data);
  }

  checkIfAuctionIsTaked = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'check_if_auction_is_taked.php'},callback,data);
  }

  acceptAuction = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'accept_auction.php'},callback,data);
  }

  setAuctionAsExpired = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'set_auction_as_expired.php'},callback,data);
  }

  registrateBuy = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'registrate_buy.php'},callback,data);
  }

  getSubscriptionReload = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_subscription_reload.php'},callback,data);
  }

  getSubscription = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_subscription.php'},callback,data);
  }

  getOrderStatus = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_order_status.php'},callback,data);
  }

  cancelOrder = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'cancel_order.php'},callback,data);
  }

  setGretting = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'set_gretting.php'},callback,data);
  }

  getOrder = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_order.php'},callback,data);
  }

  getActiveOrder = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_active_order.php'},callback,data);
  }

  getRefferPerUser = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_reffer_per_user.php'},callback,data);
  }

  saveRefferPerUser = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'save_reffer_per_user.php'},callback,data);
  }

  confirmOrder = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'confirm_order.php'},callback,data);
  }

  getCommercesWithPrewelcome = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_commerces_with_prewelcome.php'},callback,data);
  }

  savePercentajeShareCommerce = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_save_percentaje_share_menu.php'},callback,data);
  }

  saveShareCommerce = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_save_share_menu.php'},callback,data);
  }

  getForShareMenu = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_commerces_for_share_menu.php'},callback,data);
  }

  setRequestPerProduct = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'set_request_per_product.php'},callback,data);
  }

  getCarPerUser  = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_car_per_user.php'},callback,data);
  }

  saveCarPerUser  = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'save_car_per_user.php'},callback,data);
  }

  getAllvaletParking  = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_all_valete_parking.php'},callback,data);
  }

  getCatalogModelCar  = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_catalog_model_car.php'},callback,data);
  }

  getCatalogBrandCar  = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_catalog_brand_car.php'},callback,data);
  }

  saveUserRegistrationId = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'save_registration_id.php'},callback,data);
  }

  doCheckIn = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'do_check_in.php'},callback,data);
  }

  getFaqs = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_faqs.php'},callback,data);
  }

  getWalletReffer = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_wallet_reffer.php'},callback,data);
  }

  getWallet = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_wallet.php'},callback,data);
  }

  getUserCoins = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_user_coins.php'},callback,data);
  }

  getUsersAround = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_users_around.php'},callback,data);
  }

  getUsersAroundForFood = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_users_around_for_food.php'},callback,data);
  }

  getEspecialOffers = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'get_special_offers.php'},callback,data);
  }

  getSpecialData = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'load_special_data.php'},callback,data);
  }

  setDefaults = (callback: any) : any => {
		new Promise<string>((resolve,reject)=>{
	  	if(this.userObject)
	  	{	
		    this.names = (this.userObject.user_login.names) ? this.userObject.user_login.names : this.names;
		    this.mail = (this.userObject.user_login.mail) ? this.userObject.user_login.mail : this.mail;

				if(this.userObject.user_settings != undefined)
				{
		    	this.kind = (this.userObject.user_settings.kind) ? this.userObject.user_settings.kind : this.kind;
			    this.birthdate = (this.userObject.user_settings.birthdate) ? this.userObject.user_settings.birthdate : this.birthdate;
			    this.tracking_time = (this.userObject.user_settings.tracking_time) ? this.userObject.user_settings.tracking_time : this.tracking_time;
			    this.image = (this.userObject.user_settings.image) ? this.userObject.user_settings.image : this.image;
			    this.is_able_to_notification = (this.userObject.user_settings.is_able_to_notification) ? this.userObject.user_settings.is_able_to_notification : this.is_able_to_notification;
			    this.is_able_to_tracking = (this.userObject.user_settings.is_able_to_tracking) ? this.userObject.user_settings.is_able_to_tracking : this.is_able_to_tracking;
			    this.subscription = (this.userObject.user_settings.subscription) ? this.userObject.user_settings.subscription : this.subscription;
			    this.commerces_near = (this.userObject.commerces_near) ? this.userObject.commerces_near : this.commerces_near;
			    this.commerce_status = (this.userObject.commerce_status) ? this.userObject.commerce_status : this.commerce_status;
			    this.commerce_id = (this.userObject.commerce_id) ? this.userObject.commerce_id : this.commerce_id;
			    this.short_name = (this.userObject.user_login.short_name) ? this.userObject.user_login.short_name : this.short_name;
			    this.wallet = (this.userObject.user_settings.wallet) ? this.userObject.user_settings.wallet : this.wallet;
			    this.reward = (this.userObject.user_settings.reward) ? this.userObject.user_settings.reward : this.reward;

			    if(this.kind == 3)
			    {
			      this.range = (this.userObject.user_settings.range) ? this.userObject.user_settings.range : this.range;
			    }

			    if(this.kind == 1)
			    {
			      this.names = 'Comercio';
            this.short_name = 'Comercio';
			      this.commerce_type = (this.userObject.user_settings.commerce_type) ? this.userObject.user_settings.commerce_type : this.commerce_type;
			    } 
				}
	   
		    this.loadPages(()=>{
		      if(callback != undefined) callback(true);
		    })
		    resolve();
	  	}

	  	reject();
		});
  }

  loadPages = (callback: any) : any => {
    
    this.pages = [
      { title: 'Welcome', icon:'welcome',component: 'Foodhomeuser',logout:false, subscription:0, showBadge:false, badge:1, additionalData: false,root:true},
      { title: 'Comercios', icon:'near-map',component: 'StoresHome',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      // { title: 'Testing S&W', icon:'wallet',component: 'SpinandwinPage',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      { title: 'Billetera Electrónica', icon:'wallet',component: 'Ewallet',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      { title: 'Valet Parking', icon:'valete-parking',component: 'Parkinguser',logout:false, subscription:0, showBadge:false, badge:1, additionalData: false,root:true},
      { title: 'Añadir negocio', icon:'welcome',component: 'AddFastCommercePage',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      { title: 'Speech', icon:'welcome',component: 'SpeechPage',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      { title: 'Instat Chat', icon:'welcome',component: 'ChatPage',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      // { title: 'Pre Welcome', icon:'welcome',component: 'Prewelcome',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      // { title: 'Programa de Referidos', icon:'reffer',component: 'Refferuser',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      // { title: 'Subasta tu Consumo en Grupo', icon:'group',component: 'Auctionuser',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      // { title: 'Idioma', icon:'group',component: 'Auctionuser',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      { title: 'Preguntas Frecuentes', icon:'help',component: 'Faq',logout:false, subscription:1, showBadge:false, badge:1,additionalData: false,root:true},
      { title: 'Menú de App', icon:'settings',component: 'Account',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
      { title: 'Salir', icon:'log-out',component: 'Home',logout:true, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
    ];
  
    if(callback != undefined) callback();
  }

  getUser = (callback: any) : any => {
    this.storage.get("userObject").then(
      data => {
        if(data != null)
        {
          this.userObject = data;
          this.logged = true;

          this.setDefaults((response)=>{
            if(callback != undefined) callback(true);
          })

        } else {
          if(callback != undefined) callback(false);
        }
      }
    );
  }

  singUpNewUser = (callback: any,data: any) : any => {
    return this.routes.getProvider({url:'singup_user.php'},callback,data);
  }

  saveRegistration = (welcomepush:any,callback:any) : any => {
    if(this.platform.is('android') || this.platform.is('ios'))
    {
      this.platform.ready().then(() => {

        this.oneSignal.startInit(this.oneSignalProyectId, this.googleAppId);
        this.oneSignal.setSubscription(true);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
        });

        this.oneSignal.endInit();

        this.oneSignal.getIds().then(ids => {
          this.registration_id = ids.userId;
          this.saveUserRegistrationId((response)=>{
            callback(response);
          },{welcomepush:welcomepush,mail:this.userObject.user_login.mail,password:this.userObject.user_login.password,registration_id: ids.userId});
        });
      });
    } else { // must be web
      callback({s:1,r:"DATA_OK"});
    }
  }

  showRequestForLocalization = (title:any,message:any,callback:any) : any => {
    title = title ? title : 'Activar tú ubicación';
    message = message ? message : '¡Busca negocios cercanos!';
    let confirm = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            callback(false);
          }
        },
        {
          text: 'Activar',
          handler: () => {
            callback(true);
          }
        }
      ]
    });
    confirm.present();
  }

  iniPush(){
    this.platform.ready().then(() => {

      if(this.platform.is('android') || this.platform.is('ios'))
      {
        this.oneSignal.startInit(this.oneSignalProyectId, this.googleAppId);
        this.oneSignal.inFocusDisplaying(this.OneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.setSubscription(true);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
        // do something when the notification is received.
        });
        // this.oneSignal.endInit();
      }
    });
  }

  // showImage = (url:string,title:string) : any => {
  //   this.photoViewer.show(url,title);
  // }
}

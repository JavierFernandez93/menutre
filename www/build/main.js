webpackJsonp([13],{

/***/ 124:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 124;

/***/ }),

/***/ 175:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/cards/cards.module": [
		308,
		12
	],
	"../pages/content/content.module": [
		309,
		11
	],
	"../pages/item-create/item-create.module": [
		307,
		10
	],
	"../pages/item-detail/item-detail.module": [
		311,
		9
	],
	"../pages/list-master/list-master.module": [
		310,
		8
	],
	"../pages/login/login.module": [
		312,
		7
	],
	"../pages/menu/menu.module": [
		313,
		6
	],
	"../pages/search/search.module": [
		314,
		5
	],
	"../pages/settings/settings.module": [
		315,
		4
	],
	"../pages/signup/signup.module": [
		316,
		3
	],
	"../pages/tabs/tabs.module": [
		317,
		2
	],
	"../pages/tutorial/tutorial.module": [
		318,
		1
	],
	"../pages/welcome/welcome.module": [
		319,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 175;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Items; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_item__ = __webpack_require__(286);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Items = (function () {
    function Items() {
        this.itemsDone = false;
        this.percentaje = 0;
        this.items = [];
        this.defaultItem = {
            "name": "5 Plátanos para hoy",
            "profilePic": "https://estaticos.muyinteresante.es/media/cache/1000x460_thumb/uploads/images/gallery/579f2df65bafe8178b8b456d/platano2.jpg",
            "about": "Esté alimento proporciona potasio.",
            "done": false
        };
        var items = [
            {
                "name": "5 Plátanos para hoy",
                "profilePic": "https://estaticos.muyinteresante.es/media/cache/1000x460_thumb/uploads/images/gallery/579f2df65bafe8178b8b456d/platano2.jpg",
                "about": "Un plátano de unos 100 gramos contiene aproximadamente 100 calorías, 1 gramo de proteína, 25 gramos de hidratos de carbono y menos de un gramo de grasa. También contiene 3 gramos de fibra. El plátano es rico en vitaminas B6 y C, ácido fólico y minerales como el magnesio y el potasio.",
                "done": false
            },
            {
                "name": "1 Manzana al día",
                "profilePic": "http://muyenforma.com/wp-content/uploads/2015/02/dieta-manzana.jpg",
                "about": "1. Las manzanas contienen una fibra soluble llamada pectina, que ayuda a reducir el colesterol en la sangre, y evita que se acumule en las paredes de los vasos sanguíneos. 2. También contienen grandes cantidades de potasio, mineral que ayuda a controlar la presión arterial, y puede reducir el riesgo de un ataque cardiaco. 3. La quercetina es un poderoso antioxidante que también se encuentra en las manzanas, y está demostrado que ayuda a proteger las neuronas del cerebro humano. 4. Al fermentarse en el colón, la fibra natural de las manzanas produce químicos que ayudan a evitar la formación de células cancerígenas. 5. Su consumo también puede ayudar a las personas que sufren de asma. Los flavonoides y ácidos fenólicos que contiene, ayudan a calmar la inflamación de las vías respiratorias. 6. Otro de sus flavonoides, llamado floridzina, protege los huesos de enfermedades como la osteoporosis, y puede incrementar su fortalecimiento y densidad. 7. Es considerada una fruta diurética, debido a la concentración de potasio que contiene. Muy recomendable para personas que sufren de retención de líquidos. 8. Además, se recomienda su consumo para prevenir el estreñimiento o la diarrea. La cáscara de manzana contiene una fibra insoluble que ayuda la evacuación intestinal. 9. Ayuda a que los dientes se vean más limpios, porque al comerla, barre restos de otros alimentos. También se dice que es muy buena para prevenir la caries.",
                "done": false
            },
            {
                "name": "7 Almendras para este día",
                "profilePic": "http://sm.askmen.com/askmen_latam/photo/default/almendras-alimentos-para-bajar-la-ansiedad-bajar-de-peso_pdga.jpg",
                "about": "En general todas las oleaginosas, son alimentos que nos aportan importantes cantidades de fibra, proteínas, minerales, vitaminas del grupo B, vitamina E, y grasas saludables. Entre los minerales destacan hierro, fósforo, magnesio, potasio, zinc y calcio..",
                "done": false
            },
            {
                "name": "1 Pescado que delicia",
                "profilePic": "https://cdn.shopify.com/s/files/1/0555/6477/files/Embelle-TratamientoGOLD-Recetasconpescado_grande.jpg?14057681501268934163",
                "about": "Rico en proteínas de muy alto valor nutritivo aportándonos todos los aminoácidos esenciales necesarios para formar y mantener los órganos, tejidos y el sistema de defensa frente a infecciones y agentes externos.",
                "done": false
            },
            {
                "name": "Un vaso de Leche",
                "profilePic": "http://walac.pe/wp-content/uploads/2017/06/leche-segundoenfoque.jpg",
                "about": "La riqueza de las propiedades de la leche. La leche, al contrario que otras bebidas derivadas de vegetales, presenta una variedad nutricional muy rica porque contiene calcio, fósforo, magnesio, zinc, yodo, selenio y vitaminas A, D y del complejo B; presenta una cantidad muy alta de vitamina B12.",
                "done": false
            },
            {
                "name": "1 Manzana al día",
                "profilePic": "http://muyenforma.com/wp-content/uploads/2015/02/dieta-manzana.jpg",
                "about": "1. Las manzanas contienen una fibra soluble llamada pectina, que ayuda a reducir el colesterol en la sangre, y evita que se acumule en las paredes de los vasos sanguíneos. 2. También contienen grandes cantidades de potasio, mineral que ayuda a controlar la presión arterial, y puede reducir el riesgo de un ataque cardiaco. 3. La quercetina es un poderoso antioxidante que también se encuentra en las manzanas, y está demostrado que ayuda a proteger las neuronas del cerebro humano. 4. Al fermentarse en el colón, la fibra natural de las manzanas produce químicos que ayudan a evitar la formación de células cancerígenas. 5. Su consumo también puede ayudar a las personas que sufren de asma. Los flavonoides y ácidos fenólicos que contiene, ayudan a calmar la inflamación de las vías respiratorias. 6. Otro de sus flavonoides, llamado floridzina, protege los huesos de enfermedades como la osteoporosis, y puede incrementar su fortalecimiento y densidad. 7. Es considerada una fruta diurética, debido a la concentración de potasio que contiene. Muy recomendable para personas que sufren de retención de líquidos. 8. Además, se recomienda su consumo para prevenir el estreñimiento o la diarrea. La cáscara de manzana contiene una fibra insoluble que ayuda la evacuación intestinal. 9. Ayuda a que los dientes se vean más limpios, porque al comerla, barre restos de otros alimentos. También se dice que es muy buena para prevenir la caries.",
                "done": false
            },
            {
                "name": "7 Almendras para este día",
                "profilePic": "http://sm.askmen.com/askmen_latam/photo/default/almendras-alimentos-para-bajar-la-ansiedad-bajar-de-peso_pdga.jpg",
                "about": "En general todas las oleaginosas, son alimentos que nos aportan importantes cantidades de fibra, proteínas, minerales, vitaminas del grupo B, vitamina E, y grasas saludables. Entre los minerales destacan hierro, fósforo, magnesio, potasio, zinc y calcio..",
                "done": false
            },
            {
                "name": "1 Pescado que delicia",
                "profilePic": "https://cdn.shopify.com/s/files/1/0555/6477/files/Embelle-TratamientoGOLD-Recetasconpescado_grande.jpg?14057681501268934163",
                "about": "Rico en proteínas de muy alto valor nutritivo aportándonos todos los aminoácidos esenciales necesarios para formar y mantener los órganos, tejidos y el sistema de defensa frente a infecciones y agentes externos.",
                "done": false
            },
            {
                "name": "Un vaso de Leche",
                "profilePic": "http://walac.pe/wp-content/uploads/2017/06/leche-segundoenfoque.jpg",
                "about": "La riqueza de las propiedades de la leche. La leche, al contrario que otras bebidas derivadas de vegetales, presenta una variedad nutricional muy rica porque contiene calcio, fósforo, magnesio, zinc, yodo, selenio y vitaminas A, D y del complejo B; presenta una cantidad muy alta de vitamina B12.",
                "done": false
            },
            {
                "name": "1 Manzana al día",
                "profilePic": "http://muyenforma.com/wp-content/uploads/2015/02/dieta-manzana.jpg",
                "about": "1. Las manzanas contienen una fibra soluble llamada pectina, que ayuda a reducir el colesterol en la sangre, y evita que se acumule en las paredes de los vasos sanguíneos. 2. También contienen grandes cantidades de potasio, mineral que ayuda a controlar la presión arterial, y puede reducir el riesgo de un ataque cardiaco. 3. La quercetina es un poderoso antioxidante que también se encuentra en las manzanas, y está demostrado que ayuda a proteger las neuronas del cerebro humano. 4. Al fermentarse en el colón, la fibra natural de las manzanas produce químicos que ayudan a evitar la formación de células cancerígenas. 5. Su consumo también puede ayudar a las personas que sufren de asma. Los flavonoides y ácidos fenólicos que contiene, ayudan a calmar la inflamación de las vías respiratorias. 6. Otro de sus flavonoides, llamado floridzina, protege los huesos de enfermedades como la osteoporosis, y puede incrementar su fortalecimiento y densidad. 7. Es considerada una fruta diurética, debido a la concentración de potasio que contiene. Muy recomendable para personas que sufren de retención de líquidos. 8. Además, se recomienda su consumo para prevenir el estreñimiento o la diarrea. La cáscara de manzana contiene una fibra insoluble que ayuda la evacuación intestinal. 9. Ayuda a que los dientes se vean más limpios, porque al comerla, barre restos de otros alimentos. También se dice que es muy buena para prevenir la caries.",
                "done": false
            },
            {
                "name": "7 Almendras para este día",
                "profilePic": "http://sm.askmen.com/askmen_latam/photo/default/almendras-alimentos-para-bajar-la-ansiedad-bajar-de-peso_pdga.jpg",
                "about": "En general todas las oleaginosas, son alimentos que nos aportan importantes cantidades de fibra, proteínas, minerales, vitaminas del grupo B, vitamina E, y grasas saludables. Entre los minerales destacan hierro, fósforo, magnesio, potasio, zinc y calcio..",
                "done": false
            },
            {
                "name": "1 Pescado que delicia",
                "profilePic": "https://cdn.shopify.com/s/files/1/0555/6477/files/Embelle-TratamientoGOLD-Recetasconpescado_grande.jpg?14057681501268934163",
                "about": "Rico en proteínas de muy alto valor nutritivo aportándonos todos los aminoácidos esenciales necesarios para formar y mantener los órganos, tejidos y el sistema de defensa frente a infecciones y agentes externos.",
                "done": false
            },
            {
                "name": "Un vaso de Leche",
                "profilePic": "http://walac.pe/wp-content/uploads/2017/06/leche-segundoenfoque.jpg",
                "about": "La riqueza de las propiedades de la leche. La leche, al contrario que otras bebidas derivadas de vegetales, presenta una variedad nutricional muy rica porque contiene calcio, fósforo, magnesio, zinc, yodo, selenio y vitaminas A, D y del complejo B; presenta una cantidad muy alta de vitamina B12.",
                "done": false
            },
        ];
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            this.items.push(new __WEBPACK_IMPORTED_MODULE_1__models_item__["a" /* Item */](item));
        }
        this.getPercentaje();
    }
    Items.prototype.query = function (params) {
        if (!params) {
            return this.items;
        }
        return this.items.filter(function (item) {
            for (var key in params) {
                var field = item[key];
                if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
                    return item;
                }
                else if (field == params[key]) {
                    return item;
                }
            }
            return null;
        });
    };
    Items.prototype.countDone = function () {
        var done = this.items.filter(function (item) {
            return item['done'] ? item : false;
        });
        this.itemsDone = done.length;
    };
    Items.prototype.getPercentaje = function () {
        this.countDone();
        this.percentaje = (this.itemsDone * 100) / this.items.length;
    };
    Items.prototype.add = function (item) {
        this.items.push(item);
    };
    Items.prototype.isDone = function () {
        var done = this.items.filter(function (item) {
            return item['done'] ? item : false;
        });
        if (done.length == this.items.length)
            return true;
        return false;
    };
    Items.prototype.delete = function (item) {
        this.items.splice(this.items.indexOf(item), 1);
    };
    Items.prototype.done = function (item) {
        this.items[this.items.indexOf(item)]['done'] = true;
    };
    return Items;
}());
Items = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], Items);

//# sourceMappingURL=items.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Routes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the RoutesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var Routes = (function () {
    function Routes(http) {
        this.http = http;
        this.protocol = 'http';
        this.path = 'localhost:8888/welcomeweb/app/application';
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
    }
    Routes.prototype.getProvider = function (call, callback, data) {
        if (call != undefined) {
            this.http.post(this.protocol + '://' + this.path + '/' + call.url, JSON.stringify(data), { headers: this.headers }).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (typeof callback == 'function')
                    callback(data);
            });
        }
    };
    return Routes;
}());
Routes = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], Routes);

//# sourceMappingURL=routes.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstRunPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MainPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Tab1Root; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return Tab2Root; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return Tab3Root; });
// The page the user lands on after opening the app and without a session
// The page the user lands on after opening the app and without a session
var FirstRunPage = 'TutorialPage';
// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
var MainPage = 'TabsPage';
// The initial root pages for our tabs (remove if not using tabs)
var Tab1Root = 'ListMasterPage';
var Tab2Root = 'SearchPage';
var Tab3Root = 'SettingsPage';
//# sourceMappingURL=pages.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(234);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* unused harmony export provideSettings */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_diagnostic__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ngx_translate_http_loader__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ionic_angular__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_routes_routes__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__mocks_providers_items__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_providers__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__app_component__ = __webpack_require__(306);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_11__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
function provideSettings(storage) {
    /**
     * The Settings provider takes a set of default settings for your app.
     *
     * You can add new settings options at any time. Once the settings are saved,
     * these values will not overwrite the saved values (this can be done manually if desired).
     */
    return new __WEBPACK_IMPORTED_MODULE_15__providers_providers__["c" /* Settings */](storage, {
        option1: true,
        option2: 'Ionitron J. Framework',
        option3: '3',
        option4: 'Hello'
    });
}
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_16__app_component__["a" /* MyApp */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                loader: {
                    provide: __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__["a" /* TranslateLoader */],
                    useFactory: (createTranslateLoader),
                    deps: [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]]
                }
            }),
            __WEBPACK_IMPORTED_MODULE_12_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_16__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/item-create/item-create.module#ItemCreatePageModule', name: 'ItemCreatePage', segment: 'item-create', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/cards/cards.module#CardsPageModule', name: 'CardsPage', segment: 'cards', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/content/content.module#ContentPageModule', name: 'ContentPage', segment: 'content', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/list-master/list-master.module#ListMasterPageModule', name: 'ListMasterPage', segment: 'list-master', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/item-detail/item-detail.module#ItemDetailPageModule', name: 'ItemDetailPage', segment: 'item-detail', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/tutorial/tutorial.module#TutorialPageModule', name: 'TutorialPage', segment: 'tutorial', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/welcome/welcome.module#WelcomePageModule', name: 'WelcomePage', segment: 'welcome', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["a" /* IonicStorageModule */].forRoot()
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_12_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_16__app_component__["a" /* MyApp */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_15__providers_providers__["a" /* Api */],
            __WEBPACK_IMPORTED_MODULE_14__mocks_providers_items__["a" /* Items */],
            __WEBPACK_IMPORTED_MODULE_15__providers_providers__["d" /* User */],
            __WEBPACK_IMPORTED_MODULE_13__providers_routes_routes__["a" /* Routes */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
            { provide: __WEBPACK_IMPORTED_MODULE_15__providers_providers__["c" /* Settings */], useFactory: provideSettings, deps: [__WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */]] },
            // Keep this to enable Ionic's runtime error handling during development
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_12_ionic_angular__["d" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Api; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Api is a generic REST Api handler. Set your API url first.
 */
var Api = (function () {
    function Api(http) {
        this.http = http;
        this.url = 'https://example.com/api/v1';
    }
    Api.prototype.get = function (endpoint, params, reqOpts) {
        if (!reqOpts) {
            reqOpts = {
                params: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            };
        }
        // Support easy query params for GET requests
        if (params) {
            reqOpts.params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]();
            for (var k in params) {
                reqOpts.params.set(k, params[k]);
            }
        }
        return this.http.get(this.url + '/' + endpoint, reqOpts);
    };
    Api.prototype.post = function (endpoint, body, reqOpts) {
        return this.http.post(this.url + '/' + endpoint, body, reqOpts);
    };
    Api.prototype.put = function (endpoint, body, reqOpts) {
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    };
    Api.prototype.delete = function (endpoint, reqOpts) {
        return this.http.delete(this.url + '/' + endpoint, reqOpts);
    };
    Api.prototype.patch = function (endpoint, body, reqOpts) {
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    };
    return Api;
}());
Api = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
], Api);

//# sourceMappingURL=api.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Item; });
/**
 * A generic model that our Master-Detail pages list, create, and delete.
 *
 * Change "Item" to the noun your app will use. For example, a "Contact," or a
 * "Customer," or a "Animal," or something like that.
 *
 * The Items service manages creating instances of Item, so go ahead and rename
 * that something that fits your app as well.
 */
var Item = (function () {
    function Item(fields) {
        // Quick and dirty extend/assign fields to this model
        for (var f in fields) {
            this[f] = fields[f];
        }
    }
    return Item;
}());

//# sourceMappingURL=item.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Settings; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
var Settings = (function () {
    function Settings(storage, defaults) {
        this.storage = storage;
        this.SETTINGS_KEY = '_settings';
        this._defaults = defaults;
    }
    Settings.prototype.load = function () {
        var _this = this;
        return this.storage.get(this.SETTINGS_KEY).then(function (value) {
            if (value) {
                _this.settings = value;
                return _this._mergeDefaults(_this._defaults);
            }
            else {
                return _this.setAll(_this._defaults).then(function (val) {
                    _this.settings = val;
                });
            }
        });
    };
    Settings.prototype._mergeDefaults = function (defaults) {
        for (var k in defaults) {
            if (!(k in this.settings)) {
                this.settings[k] = defaults[k];
            }
        }
        return this.setAll(this.settings);
    };
    Settings.prototype.merge = function (settings) {
        for (var k in settings) {
            this.settings[k] = settings[k];
        }
        return this.save();
    };
    Settings.prototype.setValue = function (key, value) {
        this.settings[key] = value;
        return this.storage.set(this.SETTINGS_KEY, this.settings);
    };
    Settings.prototype.setAll = function (value) {
        return this.storage.set(this.SETTINGS_KEY, value);
    };
    Settings.prototype.getValue = function (key) {
        return this.storage.get(this.SETTINGS_KEY)
            .then(function (settings) {
            return settings[key];
        });
    };
    Settings.prototype.save = function () {
        return this.setAll(this.settings);
    };
    Object.defineProperty(Settings.prototype, "allSettings", {
        get: function () {
            return this.settings;
        },
        enumerable: true,
        configurable: true
    });
    return Settings;
}());
Settings = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], Object])
], Settings);

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__routes_routes__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_diagnostic__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { PhotoViewer } from '@ionic-native/photo-viewer'




var User = (function () {
    function User(loadingCtrl, storage, platform, alertCtrl, diagnostic, oneSignal, http, routes) {
        var _this = this;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.diagnostic = diagnostic;
        this.oneSignal = oneSignal;
        this.http = http;
        this.routes = routes;
        this.x = false;
        this.OneSignal = false;
        this.oneSignalProyectId = false;
        this.tap_max = 3;
        this.tap = 0;
        this.googleAppId = false;
        this.logged = false;
        this.registration_id = false;
        this.userObject = false;
        /* fast vars */
        this.is_able_to_notification = false;
        this.is_able_to_tracking = false;
        this.tracking_time = 10000;
        this.instant_chat_configured = false;
        this.subscription = false;
        this.birthdate = false;
        this.short_name = false;
        this.commerces_near = false;
        this.gold_coin = false;
        this.range = false;
        this.commerce_status = 0;
        this.commerce_type = 0;
        this.reward = 0;
        this.wallet = 0;
        this.kind = false;
        this.silver_coin = false;
        this.image = false;
        this.commerce_id = false;
        this.names = false;
        this.mail = false;
        this.responseOneSignal = false;
        this.isLogged = function () {
            _this.getUser(function () { });
            return _this.logged;
        };
        this.getAllForMap = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_commerces_for_map.php' }, callback, data);
        };
        this.setInstantChatHasConfigured = function () {
            _this.storage.set("instant_chat_configured", true).then(function () { });
        };
        this.hasInstantChatConfigured = function (callback) {
            _this.storage.get("instant_chat_configured").then(function (instant_chat_configured) {
                _this.instant_chat_configured = instant_chat_configured ? instant_chat_configured : _this.instant_chat_configured;
                if (callback != undefined)
                    callback(_this.instant_chat_configured);
            });
        };
        this.setAmountOfCommerces = function (commerces_near, callback) {
            _this.storage.get("userObject").then(function (data) {
                _this.commerces_near = commerces_near;
                data.commerces_near = commerces_near;
                _this.setUser(data, function (reponse) {
                    callback(true);
                });
            });
        };
        this.getAccountCreated = function (callback) {
            _this.storage.get("account_created").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(true);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setTutoAsViewed = function (callback, tuto) {
            _this.storage.set(tuto, true).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getTutoAsViewed = function (callback, tuto) {
            _this.storage.get(tuto).then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.getAllPromoMine = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_my_commerces.php' }, callback, data);
        };
        this.setCommerceStatus = function (callback, status) {
            _this.storage.set("commerce_active", status).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getCommerceStatus = function (callback) {
            _this.storage.get("commerce_active").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setAccountCreated = function (callback) {
            _this.storage.set("account_created", true).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getAccountEmail = function (callback) {
            _this.storage.get("email").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setSpecialDish = function (dish, callback) {
            _this.getSpecialDishes(function (response) {
                var dishes = [];
                if (response) {
                    dishes = response;
                }
                dishes.push(dish);
                _this.storage.set("dishes", dishes).then(function () {
                    if (callback != undefined)
                        callback(true);
                });
            });
        };
        this.getSpecialDishes = function (callback) {
            _this.storage.get("dishes").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setDishOrderPerUser = function (dish_order_per_user, callback) {
            _this.storage.set("dish_order_per_user", dish_order_per_user).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getDishOrderPerUser = function (callback) {
            _this.storage.get("dish_order_per_user").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setOrderPerUserId = function (order_per_user_id, callback) {
            _this.storage.set("order_per_user_id", order_per_user_id).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getOrderPerUserId = function (callback) {
            _this.storage.get("order_per_user_id").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setPrewelcome = function (callback) {
            _this.storage.set("prewelcome", true).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.deleteOrderPerUserId = function (callback) {
            _this.storage.set("order_per_user_id", false).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.deletePrewelcome = function (callback) {
            _this.storage.set("prewelcome", false).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getPrewelcome = function (callback) {
            _this.storage.get("prewelcome").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setAccountEmail = function (email, callback) {
            _this.storage.set("email", email).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.setUser = function (data, callback) {
            _this.storage.set("userObject", data).then(function () {
                _this.setAccountCreated(function (response) {
                    _this.setAccountEmail(data.user_login.mail, function (response) {
                        _this.userObject = data;
                        _this.logged = true;
                        _this.setDefaults(function (response) {
                            if (callback != undefined)
                                callback(_this.userObject);
                        });
                    });
                });
            });
        };
        this.setUserInBoardFromPrewelcome = function (callback, data) {
            return _this.routes.getProvider({ url: 'set_user_in_a_board_from_prewelcome.php' }, callback, data);
        };
        this.acceptCommerceAuction = function (callback, data) {
            return _this.routes.getProvider({ url: 'accept_auction_per_commerce.php' }, callback, data);
        };
        this.getPaymentLog = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_payment_log.php' }, callback, data);
        };
        this.loginWithFacebook = function (callback, data) {
            return _this.routes.getProvider({ url: 'login_user_with_facebook.php' }, callback, data);
        };
        this.loginUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'login_user.php' }, callback, data);
        };
        this.updateUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'update_user.php' }, callback, data);
        };
        this.updateUserPhoto = function (callback, data) {
            return _this.routes.getProvider({ url: 'update_user_photo.php' }, callback, data);
        };
        this.deleteDish = function (callback, data) {
            return _this.routes.getProvider({ url: 'delete_dish_user.php' }, callback, data);
        };
        this.saveBuyForReload = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_buy_for_reload.php' }, callback, data);
        };
        this.getCommercesForSearch = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_commerces_for_search.php' }, callback, data);
        };
        this.getDishesByKind = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_dishes_by_kind.php' }, callback, data);
        };
        this.getAmountOfDinner = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_amount_of_dinner.php' }, callback, data);
        };
        this.getAllMyFabs = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_my_fabs.php' }, callback, data);
        };
        this.getAllForSearch = function (callback, data) {
            return _this.routes.getProvider({ url: 'make_fab.php' }, callback, data);
        };
        this.makeFab = function (callback, data) {
            return _this.routes.getProvider({ url: 'make_fab.php' }, callback, data);
        };
        this.getAllMyHistory = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_my_history.php' }, callback, data);
        };
        this.registratePayPalBuy = function (callback, data) {
            return _this.routes.getProvider({ url: 'registrate_paypal_buy.php' }, callback, data);
        };
        this.addToCart = function (callback, data) {
            return _this.routes.getProvider({ url: 'add_dish_to_cart.php' }, callback, data);
        };
        this.saveFastCommerce = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_fast_commerce.php' }, callback, data);
        };
        this.saveBuy = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_buy.php' }, callback, data);
        };
        this.makeUserAuction = function (callback, data) {
            return _this.routes.getProvider({ url: 'make_auction_user.php' }, callback, data);
        };
        this.deleteAuction = function (callback, data) {
            return _this.routes.getProvider({ url: 'delete_auction.php' }, callback, data);
        };
        this.callToWaiter = function (callback, data) {
            return _this.routes.getProvider({ url: 'call_to_waiter.php' }, callback, data);
        };
        this.getReload = function (callback, data) {
            return _this.routes.getProvider({ url: 'regenerate_saved_reload.php' }, callback, data);
        };
        this.getBuy = function (callback, data) {
            return _this.routes.getProvider({ url: 'regenerate_saved_buy.php' }, callback, data);
        };
        this.getShortMenu = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_short_menu.php' }, callback, data);
        };
        this.setCommerceAsOpened = function (callback, data) {
            return _this.routes.getProvider({ url: 'set_commerce_as_opened.php' }, callback, data);
        };
        this.getAll = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_commerces.php' }, callback, data);
        };
        this.getAllUserAuctions = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_auction_user.php' }, callback, data);
        };
        this.updateReamingTime = function (callback, data) {
            return _this.routes.getProvider({ url: 'update_reaming_time.php' }, callback, data);
        };
        this.getCatalogActivity = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_catalog_activiy.php' }, callback, data);
        };
        this.getAllUserActiveAuctions = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_auction_user_active.php' }, callback, data);
        };
        this.resetOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'reset_order.php' }, callback, data);
        };
        this.checkIfAuctionIsTaked = function (callback, data) {
            return _this.routes.getProvider({ url: 'check_if_auction_is_taked.php' }, callback, data);
        };
        this.acceptAuction = function (callback, data) {
            return _this.routes.getProvider({ url: 'accept_auction.php' }, callback, data);
        };
        this.setAuctionAsExpired = function (callback, data) {
            return _this.routes.getProvider({ url: 'set_auction_as_expired.php' }, callback, data);
        };
        this.registrateBuy = function (callback, data) {
            return _this.routes.getProvider({ url: 'registrate_buy.php' }, callback, data);
        };
        this.getSubscriptionReload = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_subscription_reload.php' }, callback, data);
        };
        this.getSubscription = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_subscription.php' }, callback, data);
        };
        this.getOrderStatus = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_order_status.php' }, callback, data);
        };
        this.cancelOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'cancel_order.php' }, callback, data);
        };
        this.setGretting = function (callback, data) {
            return _this.routes.getProvider({ url: 'set_gretting.php' }, callback, data);
        };
        this.getOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_order.php' }, callback, data);
        };
        this.getActiveOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_active_order.php' }, callback, data);
        };
        this.getRefferPerUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_reffer_per_user.php' }, callback, data);
        };
        this.saveRefferPerUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_reffer_per_user.php' }, callback, data);
        };
        this.confirmOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'confirm_order.php' }, callback, data);
        };
        this.getCommercesWithPrewelcome = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_commerces_with_prewelcome.php' }, callback, data);
        };
        this.savePercentajeShareCommerce = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_save_percentaje_share_menu.php' }, callback, data);
        };
        this.saveShareCommerce = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_save_share_menu.php' }, callback, data);
        };
        this.getForShareMenu = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_commerces_for_share_menu.php' }, callback, data);
        };
        this.setRequestPerProduct = function (callback, data) {
            return _this.routes.getProvider({ url: 'set_request_per_product.php' }, callback, data);
        };
        this.getCarPerUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_car_per_user.php' }, callback, data);
        };
        this.saveCarPerUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_car_per_user.php' }, callback, data);
        };
        this.getAllvaletParking = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_valete_parking.php' }, callback, data);
        };
        this.getCatalogModelCar = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_catalog_model_car.php' }, callback, data);
        };
        this.getCatalogBrandCar = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_catalog_brand_car.php' }, callback, data);
        };
        this.saveUserRegistrationId = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_registration_id.php' }, callback, data);
        };
        this.doCheckIn = function (callback, data) {
            return _this.routes.getProvider({ url: 'do_check_in.php' }, callback, data);
        };
        this.getFaqs = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_faqs.php' }, callback, data);
        };
        this.getWalletReffer = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_wallet_reffer.php' }, callback, data);
        };
        this.getWallet = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_wallet.php' }, callback, data);
        };
        this.getUserCoins = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_user_coins.php' }, callback, data);
        };
        this.getUsersAround = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_users_around.php' }, callback, data);
        };
        this.getUsersAroundForFood = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_users_around_for_food.php' }, callback, data);
        };
        this.getEspecialOffers = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_special_offers.php' }, callback, data);
        };
        this.getSpecialData = function (callback, data) {
            return _this.routes.getProvider({ url: 'load_special_data.php' }, callback, data);
        };
        this.setDefaults = function (callback) {
            new Promise(function (resolve, reject) {
                if (_this.userObject) {
                    _this.names = (_this.userObject.user_login.names) ? _this.userObject.user_login.names : _this.names;
                    _this.mail = (_this.userObject.user_login.mail) ? _this.userObject.user_login.mail : _this.mail;
                    if (_this.userObject.user_settings != undefined) {
                        _this.kind = (_this.userObject.user_settings.kind) ? _this.userObject.user_settings.kind : _this.kind;
                        _this.birthdate = (_this.userObject.user_settings.birthdate) ? _this.userObject.user_settings.birthdate : _this.birthdate;
                        _this.tracking_time = (_this.userObject.user_settings.tracking_time) ? _this.userObject.user_settings.tracking_time : _this.tracking_time;
                        _this.image = (_this.userObject.user_settings.image) ? _this.userObject.user_settings.image : _this.image;
                        _this.is_able_to_notification = (_this.userObject.user_settings.is_able_to_notification) ? _this.userObject.user_settings.is_able_to_notification : _this.is_able_to_notification;
                        _this.is_able_to_tracking = (_this.userObject.user_settings.is_able_to_tracking) ? _this.userObject.user_settings.is_able_to_tracking : _this.is_able_to_tracking;
                        _this.subscription = (_this.userObject.user_settings.subscription) ? _this.userObject.user_settings.subscription : _this.subscription;
                        _this.commerces_near = (_this.userObject.commerces_near) ? _this.userObject.commerces_near : _this.commerces_near;
                        _this.commerce_status = (_this.userObject.commerce_status) ? _this.userObject.commerce_status : _this.commerce_status;
                        _this.commerce_id = (_this.userObject.commerce_id) ? _this.userObject.commerce_id : _this.commerce_id;
                        _this.short_name = (_this.userObject.user_login.short_name) ? _this.userObject.user_login.short_name : _this.short_name;
                        _this.wallet = (_this.userObject.user_settings.wallet) ? _this.userObject.user_settings.wallet : _this.wallet;
                        _this.reward = (_this.userObject.user_settings.reward) ? _this.userObject.user_settings.reward : _this.reward;
                        if (_this.kind == 3) {
                            _this.range = (_this.userObject.user_settings.range) ? _this.userObject.user_settings.range : _this.range;
                        }
                        if (_this.kind == 1) {
                            _this.names = 'Comercio';
                            _this.short_name = 'Comercio';
                            _this.commerce_type = (_this.userObject.user_settings.commerce_type) ? _this.userObject.user_settings.commerce_type : _this.commerce_type;
                        }
                    }
                    _this.loadPages(function () {
                        if (callback != undefined)
                            callback(true);
                    });
                    resolve();
                }
                reject();
            });
        };
        this.loadPages = function (callback) {
            _this.pages = [
                { title: 'Welcome', icon: 'welcome', component: 'Foodhomeuser', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                { title: 'Comercios', icon: 'near-map', component: 'StoresHome', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                // { title: 'Testing S&W', icon:'wallet',component: 'SpinandwinPage',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
                { title: 'Billetera Electrónica', icon: 'wallet', component: 'Ewallet', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                { title: 'Valet Parking', icon: 'valete-parking', component: 'Parkinguser', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                { title: 'Añadir negocio', icon: 'welcome', component: 'AddFastCommercePage', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                { title: 'Speech', icon: 'welcome', component: 'SpeechPage', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                { title: 'Instat Chat', icon: 'welcome', component: 'ChatPage', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                // { title: 'Pre Welcome', icon:'welcome',component: 'Prewelcome',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
                // { title: 'Programa de Referidos', icon:'reffer',component: 'Refferuser',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
                // { title: 'Subasta tu Consumo en Grupo', icon:'group',component: 'Auctionuser',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
                // { title: 'Idioma', icon:'group',component: 'Auctionuser',logout:false, subscription:0, showBadge:false, badge:1,additionalData: false,root:true},
                { title: 'Preguntas Frecuentes', icon: 'help', component: 'Faq', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                { title: 'Menú de App', icon: 'settings', component: 'Account', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                { title: 'Salir', icon: 'log-out', component: 'Home', logout: true, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
            ];
            if (callback != undefined)
                callback();
        };
        this.getUser = function (callback) {
            _this.storage.get("userObject").then(function (data) {
                if (data != null) {
                    _this.userObject = data;
                    _this.logged = true;
                    _this.setDefaults(function (response) {
                        if (callback != undefined)
                            callback(true);
                    });
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.singUpNewUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'singup_user.php' }, callback, data);
        };
        this.saveRegistration = function (welcomepush, callback) {
            if (_this.platform.is('android') || _this.platform.is('ios')) {
                _this.platform.ready().then(function () {
                    _this.oneSignal.startInit(_this.oneSignalProyectId, _this.googleAppId);
                    _this.oneSignal.setSubscription(true);
                    _this.oneSignal.handleNotificationReceived().subscribe(function () {
                    });
                    _this.oneSignal.endInit();
                    _this.oneSignal.getIds().then(function (ids) {
                        _this.registration_id = ids.userId;
                        _this.saveUserRegistrationId(function (response) {
                            callback(response);
                        }, { welcomepush: welcomepush, mail: _this.userObject.user_login.mail, password: _this.userObject.user_login.password, registration_id: ids.userId });
                    });
                });
            }
            else {
                callback({ s: 1, r: "DATA_OK" });
            }
        };
        this.showRequestForLocalization = function (title, message, callback) {
            title = title ? title : 'Activar tú ubicación';
            message = message ? message : '¡Busca negocios cercanos!';
            var confirm = _this.alertCtrl.create({
                title: title,
                message: message,
                buttons: [
                    {
                        text: 'Cancelar',
                        handler: function () {
                            callback(false);
                        }
                    },
                    {
                        text: 'Activar',
                        handler: function () {
                            callback(true);
                        }
                    }
                ]
            });
            confirm.present();
        };
        this.init(function (response) {
            if (response) {
                _this.saveRegistration({}, function (d) {
                });
            }
        });
    }
    User.prototype.init = function (callback) {
        this.OneSignal = false;
        this.oneSignalProyectId = "f11df8cc-74a7-48a6-869e-e5cf4e1c88b8";
        this.googleAppId = "593000169379";
        this.logged = false;
        this.birthdate = false;
        this.registration_id = 1;
        this.userObject = false;
        this.is_able_to_notification = false;
        this.is_able_to_tracking = false;
        this.subscription = false;
        this.short_name = 'Usuario';
        this.commerces_near = false;
        this.range = false;
        this.gold_coin = false;
        this.reward = 0;
        this.wallet = 0;
        this.kind = false;
        this.silver_coin = false;
        this.image = 'assets/img/upload_photo.png';
        this.names = 'Usuario';
        this.mail = false;
        this.responseOneSignal = false;
        if (callback)
            callback(true);
    };
    User.prototype.resetTap = function () {
        this.tap = 0;
    };
    User.prototype.needMiaHelp = function () {
        console.log("NEED MIA HELP");
    };
    User.prototype.signup = function (accountInfo) {
        // let seq = this.api.post('signup', accountInfo).share();
        // seq.subscribe((res: any) => {
        //   // If the API returned a successful response, mark the user as logged in
        //   if (res.status == 'success') {
        //     this._loggedIn(res);
        //   }
        // }, err => {
        //   console.error('ERROR', err);
        // });
        return true;
    };
    User.prototype.login = function (accountInfo) {
        return true;
    };
    User.prototype.tapEvent = function (event) {
        var _this = this;
        setTimeout(function () {
            _this.resetTap();
        }, 500);
        this.tap++;
        if (this.tap == this.tap_max) {
            this.needMiaHelp();
            this.resetTap();
        }
    };
    User.prototype.getCredentials = function (data) {
        var _this = this;
        var defaults = false;
        new Promise(function (resolve, reject) {
            defaults = {
                mail: _this.userObject.user_login.mail,
                password: _this.userObject.user_login.password,
                company_id: _this.userObject.user_login.company_id
            };
            if (data)
                Object.assign(defaults, data);
            resolve();
        });
        return defaults;
    };
    User.prototype.logout = function (callback) {
        var _this = this;
        this.subscription = false;
        this.commerce_id = false;
        this.kind = false;
        this.image = 'assets/img/upload_photo.png';
        this.names = 'Usuario';
        this.storage.remove('userObject').then(function () {
            _this.storage.remove('prewelcome').then(function () {
                _this.storage.remove('dish_order_per_user').then(function () {
                    _this.storage.remove('order_per_user_id').then(function () {
                        if (callback())
                            callback(true);
                    });
                });
            });
        });
    };
    User.prototype.iniPush = function () {
        var _this = this;
        this.platform.ready().then(function () {
            if (_this.platform.is('android') || _this.platform.is('ios')) {
                _this.oneSignal.startInit(_this.oneSignalProyectId, _this.googleAppId);
                _this.oneSignal.inFocusDisplaying(_this.OneSignal.OSInFocusDisplayOption.InAppAlert);
                _this.oneSignal.setSubscription(true);
                _this.oneSignal.handleNotificationReceived().subscribe(function () {
                    // do something when the notification is received.
                });
                // this.oneSignal.endInit();
            }
        });
    };
    return User;
}());
User = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_diagnostic__["a" /* Diagnostic */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__["a" /* OneSignal */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_0__routes_routes__["a" /* Routes */]])
], User);

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_splash_screen__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_pages__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_providers__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(translate, platform, settings, config, statusBar, splashScreen) {
        var _this = this;
        this.translate = translate;
        this.config = config;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_pages__["a" /* FirstRunPage */];
        this.pages = [
            { title: 'Mi progreso del día', component: 'TutorialPage' },
            { title: 'Ayuda', component: 'WelcomePage' },
            { title: 'Salir', component: 'TabsPage' },
        ];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
        this.initTranslate();
    }
    MyApp.prototype.initTranslate = function () {
        var _this = this;
        // Set the default language for translation strings, and the current language.
        this.translate.setDefaultLang('en');
        if (this.translate.getBrowserLang() !== undefined) {
            this.translate.use(this.translate.getBrowserLang());
        }
        else {
            this.translate.use('en'); // Set your language here
        }
        this.translate.get(['BACK_BUTTON_TEXT']).subscribe(function (values) {
            _this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        template: "<ion-menu [content]=\"content\">\n    <ion-header>\n      <ion-toolbar>\n        <ion-title>Cuenta de javier</ion-title>\n      </ion-toolbar>\n    </ion-header>\n\n    <ion-content>\n      <ion-list>\n        <button menuClose ion-item *ngFor=\"let p of pages\" (click)=\"openPage(p)\">\n          {{p.title}}\n        </button>\n      </ion-list>\n    </ion-content>\n\n  </ion-menu>\n  <ion-nav #content [root]=\"rootPage\"></ion-nav>"
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_6__providers_providers__["c" /* Settings */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* Config */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_api__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mocks_providers_items__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_user__ = __webpack_require__(288);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__api_api__["a"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__mocks_providers_items__["a"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_2__settings_settings__["a"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_3__user_user__["a"]; });





//# sourceMappingURL=providers.js.map

/***/ })

},[219]);
//# sourceMappingURL=main.js.map
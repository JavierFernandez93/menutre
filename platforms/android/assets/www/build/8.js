webpackJsonp([8],{

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListMasterPageModule", function() { return ListMasterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_master__ = __webpack_require__(324);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ListMasterPageModule = (function () {
    function ListMasterPageModule() {
    }
    return ListMasterPageModule;
}());
ListMasterPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__list_master__["a" /* ListMasterPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__list_master__["a" /* ListMasterPage */]),
            __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild()
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_3__list_master__["a" /* ListMasterPage */]
        ]
    })
], ListMasterPageModule);

//# sourceMappingURL=list-master.module.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListMasterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ListMasterPage = (function () {
    function ListMasterPage(navCtrl, items, modalCtrl) {
        this.navCtrl = navCtrl;
        this.items = items;
        this.modalCtrl = modalCtrl;
        this.done = false;
        this.currentItems = this.items.query();
        this.day = "viernes";
    }
    /**
     * The view loaded, let's query our items for the list
     */
    ListMasterPage.prototype.ionViewDidLoad = function () {
    };
    /**
     * Prompt the user to add a new item. This shows our ItemCreatePage in a
     * modal and then adds the new item to our data source if the user created one.
     */
    ListMasterPage.prototype.doneItem = function (item) {
        this.items.done(item);
        this.items.getPercentaje();
        this.done = this.items.isDone();
    };
    ListMasterPage.prototype.addItem = function () {
        var _this = this;
        var addModal = this.modalCtrl.create('ItemCreatePage');
        addModal.onDidDismiss(function (item) {
            if (item) {
                _this.items.add(item);
            }
        });
        addModal.present();
    };
    /**
     * Delete an item from the list of items.
     */
    ListMasterPage.prototype.deleteItem = function (item) {
        this.items.delete(item);
        this.items.getPercentaje();
    };
    /**
     * Navigate to the detail page for this item.
     */
    ListMasterPage.prototype.openItem = function (item) {
        this.navCtrl.push('ItemDetailPage', {
            item: item
        });
    };
    return ListMasterPage;
}());
ListMasterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-list-master',template:/*ion-inline-start:"/Applications/MAMP/htdocs/menutre/src/pages/list-master/list-master.html"*/'<ion-header>\n  \n  <ion-navbar>\n    <button ion-button menuToggle class="button-transparent">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ \'LIST_MASTER_TITLE\' | translate }} {{day}}</ion-title>\n\n    <!-- <ion-buttons end>\n      <button ion-button icon-only (click)="addItem()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <div *ngIf="!done">\n    <ion-row text-center>\n      <ion-col>\n        <p>Porcentaje de avance</p>\n        <h1>{{items.percentaje}} %</h1>\n\n      </ion-col>\n      <ion-col>\n        <p>Tareas completadas</p>\n        <h1>{{items.itemsDone}} de {{items.items.length}}</h1>\n      </ion-col>\n    </ion-row>\n    <ion-list>\n      <ion-item-sliding *ngFor="let item of currentItems">\n        <button ion-item (click)="openItem(item)">\n          <ion-avatar item-start>\n            <img [src]="item.profilePic" />\n          </ion-avatar>\n          \n          <h2><ion-icon *ngIf="item.done" color="secondary" name="checkmark-circle"></ion-icon> {{item.name}}</h2>\n          <p>{{item.about}}</p>\n          <ion-note item-end *ngIf="item.note">{{item.note}}</ion-note>\n        </button>\n\n        <ion-item-options  *ngIf="!item.done">\n          <button ion-button color="danger" (click)="deleteItem(item)">\n            {{ \'DELETE_BUTTON\' | translate }}\n          </button>\n          <button ion-button color="blue" (click)="doneItem(item)">\n            {{ \'DONE_BUTTON\' | translate }}\n          </button>\n        </ion-item-options>\n      </ion-item-sliding>\n    </ion-list>\n  </div>\n  <ion-row *ngIf="done" text-center style="padding: 22% 0;">\n    <ion-col>\n      <img src="assets/img/medal.png" style="width: 128px;">\n      <h2 ion-text>{{ \'CONGRATS_TITLE\' | translate }}</h2>\n      <h3 ion-text>{{ \'CONGRATS_SUBTITLE\' | translate }}</h3>\n    </ion-col>\n  </ion-row>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/menutre/src/pages/list-master/list-master.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_providers__["b" /* Items */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
], ListMasterPage);

//# sourceMappingURL=list-master.js.map

/***/ })

});
//# sourceMappingURL=8.js.map